package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import connection.*;
import model.*;

public class ClientiDAO extends AbstractDAO<Clienti>{
	public ArrayList<Clienti> getClient() throws Exception
	{
		try
		{
			Connection con= ConnectionFactory.getConnection();
			PreparedStatement statement= con.prepareStatement("SELECT * FROM clienti");
		
			ResultSet result= statement.executeQuery();
			
			ArrayList<Clienti> clienti= new ArrayList<Clienti>();
		
			while(result.next())
			{   int id=Integer.parseInt(result.getString("idClient"));
				String numeClient= result.getString("numeClient");
				String adresaClient= result.getString("adresaClient");
				
				
				
				Clienti client= new Clienti(id,numeClient,adresaClient);
				clienti.add(client);
				client.toString();
			}
			
			
			return clienti;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		return null;
	}
	
}
