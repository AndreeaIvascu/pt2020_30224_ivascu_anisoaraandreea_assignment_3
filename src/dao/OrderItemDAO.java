package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import connection.ConnectionFactory;
import model.*;

public class OrderItemDAO extends AbstractDAO<OrderItem>{
	public ArrayList<OrderItem> getOrderItem() throws Exception
	{
		try
		{
			Connection con= ConnectionFactory.getConnection();
			PreparedStatement statement= con.prepareStatement("SELECT * FROM OrderItem");
		
			ResultSet result= statement.executeQuery();
			
			ArrayList<OrderItem> comenzi= new ArrayList<OrderItem>();
		
			while(result.next())
			{
				
				int idOrderItem=Integer.parseInt(result.getString("idOrderItem"));
				int idProdus=Integer.parseInt(result.getString("idProdus"));
				int idOrders=Integer.parseInt(result.getString("IdClient"));
				int cantitate=Integer.parseInt(result.getString("cantitateOrderItem"));
				double pret= Double.parseDouble(result.getString("pretOrderItem"));
				
				OrderItem order= new OrderItem(idOrderItem,idProdus,idOrders,cantitate,pret);
				comenzi.add(order);

			}	
			
			return comenzi;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		return null;
	}

}
