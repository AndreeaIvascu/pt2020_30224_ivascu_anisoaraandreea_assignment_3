package dao;

import model.*;
import connection.*;
import java.sql.*;
import java.util.ArrayList;

public class ProdusDAO extends AbstractDAO<Produs>{
	
	private String createSelectPriceQueryByName(String conditie) {
		
		
		

		StringBuilder sb = new StringBuilder();

		sb.append("SELECT");
		sb.append("idProdus");
		sb.append("from");
		sb.append("Produs");
		sb.append("where nume=?");
		sb.append(conditie);

		return sb.toString();

	}

	
	
	
	public ArrayList<Produs> getProdus() throws Exception
	{
		try
		{
			Connection con= ConnectionFactory.getConnection();
			PreparedStatement statement= con.prepareStatement("SELECT * FROM produs");
		
			ResultSet result= statement.executeQuery();
			
			ArrayList<Produs> produse= new ArrayList<Produs>();
		
			while(result.next())
			{
				int id=Integer.parseInt(result.getString("idProdus"));
				String nume= result.getString("numeProdus");
				int cantitate=Integer.parseInt(result.getString("cantitateProdus"));
				double pret= Double.parseDouble(result.getString("pretProdus"));
				
				Produs produs= new Produs(id,nume,cantitate,pret);
				produse.add(produs);

			}	
		
			return produse;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		return null;
	}
	
	
	
	
	
	
	

}
