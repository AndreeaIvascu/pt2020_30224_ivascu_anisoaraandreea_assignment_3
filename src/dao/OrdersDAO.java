package dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import connection.ConnectionFactory;
import model.*;

public class OrdersDAO extends AbstractDAO<Orders>{
	
	public ArrayList<Orders> getOrders() throws Exception
	{
		try
		{
			Connection con= ConnectionFactory.getConnection();
			PreparedStatement statement= con.prepareStatement("SELECT * FROM Orders");
		
			ResultSet result= statement.executeQuery();
			
			ArrayList<Orders> comanda= new ArrayList<Orders>();
		
			while(result.next())
			{
				
				int idOrders=Integer.parseInt(result.getString("idOrders"));
				int idClient=Integer.parseInt(result.getString("idClient"));
				double pret= Double.parseDouble(result.getString("pretOrders"));
				
				Orders order= new Orders(idOrders,idClient,pret);
				comanda.add(order);

			}	

			return comanda;
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		return null;
	}

}



