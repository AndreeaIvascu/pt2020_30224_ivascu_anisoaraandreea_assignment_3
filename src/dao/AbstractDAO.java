package dao;

import java.beans.*;
import java.lang.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;

public class AbstractDAO<T> {

	private static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	private final Class<T> type;

	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	private String createSelectQuery() {

		StringBuilder sb = new StringBuilder();
		sb.append("select");
		sb.append(" * ");
		sb.append(" from ");
		sb.append(type.getSimpleName());
		return sb.toString();
	}

	private String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT");
		sb.append(" * ");
		sb.append("FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field + "=?");
		return sb.toString();
	}

	private String createDeleteQuery(String cond) {

		StringBuilder sb = new StringBuilder();
		sb.append("delete from ");
		sb.append(type.getSimpleName());
		sb.append(" where " + cond);
		//System.out.println(sb.toString());
		return sb.toString();
	}

	private String createInsertQuery(String cond) {

		StringBuilder sb = new StringBuilder();
		sb.append("insert into ");
		sb.append(type.getSimpleName());
		sb.append(" value(" + cond + ")");
		//System.out.println(sb.toString());
		return sb.toString();
	}

	public String createUpdateQuery(String edit, String cond) {
		StringBuilder sb = new StringBuilder();

		sb.append("update ");
		sb.append(type.getSimpleName());
		sb.append(" set cantitateProdus=");
		sb.append(edit);
		sb.append(" where numeProdus=");
		sb.append(cond);

		return sb.toString();
	}

	public void update(String s1, String s2) throws Exception {

		Connection connection = null;
		String query = createUpdateQuery(s1, s2);
		try {

			connection = ConnectionFactory.getConnection();
			PreparedStatement st = connection.prepareStatement(query);
			// ResultSet rs=st.executeQuery();
			st.executeUpdate();
		} catch (Exception e) {
			System.out.println("ERROR3 update");
		}

	}

	public void insert(String s) throws Exception {

		Connection connection = null;
		String query = createInsertQuery(s);
		try {

			connection = ConnectionFactory.getConnection();
			PreparedStatement st = connection.prepareStatement(query);
			// ResultSet rs=st.executeQuery();
			st.executeUpdate();
		} catch (Exception e) {
			System.out.println("ERROR1 insert");
		}

	}

	public void delete(String s) throws Exception {

		Connection connection = null;
		String query = createDeleteQuery(s);
		try {

			connection = ConnectionFactory.getConnection();
			PreparedStatement st = connection.prepareStatement(query);
			// ResultSet rs=st.executeQuery();
			st.executeUpdate();
		} catch (Exception e) {
			System.out.println("ERROR2 delete");
		}

	}

	/*
	 * public T findByName(String cauta,String nume) throws Exception{ Connection
	 * connection=null; PreparedStatement statement=null; ResultSet resultSet=null;
	 * String query=createSelectQuery(cauta); try {
	 * connection=ConnectionFactory.getConnection();
	 * statement=connection.prepareStatement(query);
	 * 
	 * statement.setString(1,nume);
	 * 
	 * resultSet=statement.executeQuery();
	 * 
	 * 
	 * 
	 * return createObjects(resultSet).get(0);
	 * 
	 * } catch(SQLException e) {
	 * LOGGER.log(Level.WARNING,type.getName()+" DAO:findByName "+e.getMessage());
	 * 
	 * } finally { ConnectionFactory.close(connection);
	 * ConnectionFactory.close(resultSet); ConnectionFactory.close(statement);
	 * 
	 * } return null;
	 * 
	 * }
	 */

	public T findByName(String nume, String cauta) throws Exception {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery(cauta);
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);

			statement.setString(1, nume);

			resultSet = statement.executeQuery();

			return createObjects(resultSet).get(0);

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + " DAO:findByName " + e.getMessage());

		} finally {

			ConnectionFactory.close(connection);
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
		}
		return null;
	}

	public T findById(int id) throws Exception {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("idClient");
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);

			statement.setInt(1, id);

			resultSet = statement.executeQuery();

			return createObjects(resultSet).get(0);

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + " DAO:findById " + e.getMessage());

		} finally {
			ConnectionFactory.close(connection);
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);

		}
		return null;

	}

	private List<T> createObjects(ResultSet resultSet) throws Exception {
		List<T> list = new ArrayList();
		try {
			while (resultSet.next()) {

				T instance = type.newInstance();

				for (Field field : type.getDeclaredFields())

				{
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);

				}

				list.add(instance);
			}
		} catch (InstantiationException e) {
			System.out.println("error createObject");
		}
		return list;
	}

	public List<T> findAll() throws Exception {
		List<T> list = new ArrayList();
		Connection connection = null;
		String query = createSelectQuery();
		try {

			connection = ConnectionFactory.getConnection();
			PreparedStatement statement = connection.prepareStatement(query);

			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}

		} catch (InstantiationException e) {
			System.out.println("error list");
		}

		return list;
	}

}
