package model;

public class OrderItem {
	
	private int idOrderItem;
	private int idProdus;
	private int idClient;
	private int cantitateOrderItem;
	private double pretOrderItem;
	
	
	
	public OrderItem(int idOrderItem, int idProdus, int idOrders, int cantitateOrderItem, double pretOrderItem) {
		
		this.idOrderItem = idOrderItem;
		this.idProdus = idProdus;
		this.idClient = idOrders;
		this.cantitateOrderItem = cantitateOrderItem;
		this.pretOrderItem = pretOrderItem;
	}
	
	
	public int getIdOrderItem() {
		return idOrderItem;
	}
	public void setIdOrderItem(int idOrderItem) {
		idOrderItem = idOrderItem;
	}
	public int getIdProdus() {
		return idProdus;
	}
	public void setIdProdus(int idProdus) {
		idProdus = idProdus;
	}
	public int getIdClient() {
		return idClient;
	}
	public void setIdOrders(int idOrders) {
		idOrders = idOrders;
	}
	public int getCantitateOrderItem() {
		return cantitateOrderItem;
	}
	public void setCantitateOrderItem(int cantitateOrderItem) {
		this.cantitateOrderItem = cantitateOrderItem;
	}
	public double getPretOrderItem() {
		return pretOrderItem;
	}
	public void setPretOrderItem(double pretOrderItem) {
		this.pretOrderItem = pretOrderItem;
	}
	

}
