package model;

public class Produs {
	
	private int idProdus;
	private String numeProdus;
	private int cantitateProdus;
	private double pretProdus;
	
	
	
	
	
	public Produs(int idProdus, String numeProdus, int cantitateProdus, double pretProdus) {
	
		this.idProdus = idProdus;
		this.numeProdus = numeProdus;
		this.cantitateProdus = cantitateProdus;
		this.pretProdus = pretProdus;
	}
	
	
	
	public int getIdProdus() {
		return idProdus;
	}
	public void setIdProdus(int idProdus) {
		idProdus = idProdus;
	}
	public String getNumeProdus() {
		return numeProdus;
	}
	public void setNumeProdus(String numeProdus) {
		this.numeProdus = numeProdus;
	}
	public int getCantitateProdus() {
		return cantitateProdus;
	}
	public void setCantitateProdus(int cantitateProdus) {
		this.cantitateProdus = cantitateProdus;
	}
	public double getPretProdus() {
		return pretProdus;
	}
	public void setPretProdus(double pretProdus) {
		this.pretProdus = pretProdus;
	}
	
	

}
