package model;

public class Orders {
	private int idOrders;
     private int idClient;
     private double pretOrders;
     
     
     
     
	public Orders(int idOrders, int idClient,double pretOrders) {
		
		this.idOrders = idOrders;
		this.idClient = idClient;
		this.pretOrders = pretOrders;
	}
	
	
	
	public int getIdOrders() {
		return idOrders;
	}
	public void setIdOrders(int idOrders) {
		this.idOrders = idOrders;
	}
	public int getIdClient() {
		return idClient;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	
	public double getPretOrders() {
		return pretOrders;
	}
	public void setPretOrders(int pretOrders) {
		this.pretOrders = pretOrders;
	}
     
     
}
