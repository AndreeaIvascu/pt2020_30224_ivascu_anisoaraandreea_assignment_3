package model;

public class Clienti {
	private int idClient;
	private String numeClient;
	private String adresaClient;
	
	
	
	
	public Clienti(int IdClient,String numeClient,String adresaClient)
	{
		this.idClient=IdClient;
		this.numeClient=numeClient;
		this.adresaClient=adresaClient;
	}
	
	
	
	
	public int getIdClient() {
		return idClient;
	}
	public void setIdClient(int idClient) {
		idClient = idClient;
	}
	public String getNumeClient() {
		return numeClient;
	}
	public void setNumeClient(String numeClient) {
		this.numeClient = numeClient;
	}
	public String getAdresaClient() {
		return adresaClient;
	}
	public void setAdresaClient(String adresaClient) {
		this.adresaClient = adresaClient;
	}
	

}
