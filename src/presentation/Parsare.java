package presentation;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import dao.*;
import model.Clienti;

import model.OrderItem;
import model.Produs;


public class Parsare {
	static ArrayList<String> requirements = new ArrayList<String>();

	ClientiDAO client = new ClientiDAO();
	ProdusDAO produs = new ProdusDAO();
	OrderItemDAO comanda = new OrderItemDAO();
	public static ArrayList<Clienti> clienti = new ArrayList<Clienti>();
	public static ArrayList<Produs> produse = new ArrayList<Produs>();
	public static ArrayList<OrderItem> comenzi = new ArrayList<OrderItem>();

	public ArrayList<Clienti> afisClienti() {
		ArrayList<Clienti> cl = new ArrayList<Clienti>();
		for (int i = 0; i < clienti.size(); i++)
			cl.add(clienti.get(i));
		return cl;

	}

	public ArrayList<Produs> afisProduse() {
		ArrayList<Produs> pr = new ArrayList<Produs>();
		for (int i = 0; i < produse.size(); i++)
			pr.add(produse.get(i));
		return pr;

	}

	public ArrayList<OrderItem> afisComenzi() {
		ArrayList<OrderItem> cm = new ArrayList<OrderItem>();
		for (int i = 0; i < comenzi.size(); i++)
			cm.add(comenzi.get(i));
		return cm;

	}

	public Parsare() throws Exception {

	}

	public int indexClient(String s) {
		int index = 0;
		for (int i = 0; i < clienti.size(); i++) {
			if (clienti.get(i).getNumeClient().compareTo(s) == 0)
				index = clienti.get(i).getIdClient();
		}
		return index;
	}

	public int indexProdus(String s) {
		int index = 0;
		for (int i = 0; i < produse.size(); i++) {
			if (produse.get(i).getNumeProdus().compareTo(s) == 0) {
				index = produse.get(i).getIdProdus();
			}

		}
		return index;
	}

	static int idClient = 1;
	static int idProdus = 1;
	static int idComanda = 1;

	public void funcCiteste(InputStreamReader fchar) throws Exception {
		BufferedReader buf = new BufferedReader(fchar);
		String citeste;
		int nrRequirements = 0;
		citeste = buf.readLine();

		while (!(citeste == null)) {
			requirements.add(citeste);
			citeste = buf.readLine();

			nrRequirements++;
		}

		String a;
		for (int i = 0; i < nrRequirements; i++) {
			a = requirements.get(i);
			//System.out.println(requirements.size());
			if (a.compareTo("Report product") == 0) {// System.out.println(a);
				// comenzi.add(a);
				ArrayList<Produs> prod = new ArrayList<Produs>();
				prod = produs.getProdus();
				GeneratePDF pf = new GeneratePDF();
				pf.raportProductPDF(prod);
			}

			if (a.compareTo("Report client") == 0) { // System.out.println(a);
				GeneratePDF pf = new GeneratePDF();
				pf.raportClientPDF(clienti); // comenzi.add(a);
			}
			
			if (a.compareTo("Report order") == 0) {// System.out.println(a);
				GeneratePDF pf = new GeneratePDF();
				pf.raportOrderPDF(comenzi); // comenzi.add(a);
			}
			if (a.indexOf("Insert client") > -1) {
				String s = (split_client(a));

				s = idClient + "," + s;
				// System.out.println(s);
				client.insert(s);
				idClient++;
			}

			if (a.indexOf("Order") > -1) {
				String s = (split_order(a));
				s = idComanda + "," + s;
				comanda.insert(s);
				idComanda++;
			}

			if (a.indexOf("Insert product") > -1) {
				String s = split_product(a);
				if (s.compareTo("nu") != 0) {
					s = idProdus + "," + s;
					produs.insert(s);
					idProdus++;
				}
			}

			if (a.indexOf("Delete client") > -1) {
				String s = (split_del_client(a));
				client.delete(s);
			}
			if (a.indexOf("Delete product") > -1) {
				String s = (split_del_product(a));
				produs.delete(s);
			}
		}

	}

	private String split_client(String s) {
		String[] values = s.split(":");
		String[] val = values[1].split(",");
		String sfinal = "";
		sfinal = "'" + val[0] + "'" + "," + "'" + val[1] + "'";
		clienti.add(new Clienti(idClient, val[0], val[1]));
		return sfinal;
	}

	private String split_product(String s) throws Exception {

		String[] values = s.split(":");
		String[] val = values[1].split(",");
		String sfinal = "";
		sfinal = "'" + val[0] + "'" + "," + val[1] + "," + val[2];

		int i = indexProdus(val[0]);
		if (i != 0) {

			String valueC = "";
			valueC = valueC + (produse.get(i - 1).getCantitateProdus() + Integer.parseInt(val[1].substring(1)));
			String cauta = "'" + val[0] + "'";
			produse.get(i - 1).setCantitateProdus(
					produse.get(i - 1).getCantitateProdus() + Integer.parseInt(val[1].substring(1)));
			produs.update(valueC, cauta);
			return "nu";
		}
		produse.add(new Produs(idProdus, val[0], Integer.parseInt(val[1].substring(1)),
				Double.parseDouble(val[2].substring(1))));
		return sfinal;
	}

	private String split_order(String s) throws Exception {

		String[] values = s.split(":");
		String[] val = values[1].split(",");
		String sfinal = "";
	

		// val[0]=val[0].substring(1);
		int i1 = indexClient(val[0]);
		// val[1]=val[1].substring(1);
		System.out.println(val[0] + " " + val[1]);
		int i2 = indexProdus(val[1]);
		double price = produse.get(i2 - 1).getPretProdus();
		price = Integer.parseInt(val[2].substring(1)) * price;
		
		 sfinal=i2+","+i1+","+val[2]+","+price;

		if (Integer.parseInt(val[2].substring(1)) < produse.get(i2 - 1).getCantitateProdus()) {

			String valueC = "";
			valueC = valueC + (produse.get(i2 - 1).getCantitateProdus() - Integer.parseInt(val[2].substring(1)));
			String cauta = "'" + val[1] + "'";
			produse.get(i2 - 1).setCantitateProdus(produse.get(i2 - 1).getCantitateProdus() - Integer.parseInt(val[2].substring(1)));
			produs.update(valueC, cauta);
			sfinal = i2 + ", " + i1 + "," + val[2] + "," + price;
			comenzi.add(new OrderItem(idComanda, i2, i1, Integer.parseInt(val[2].substring(1)), price));

			GeneratePDF pf = new GeneratePDF();
			pf.createBill(clienti.get(i1 - 1).getNumeClient(), produse.get(i2 - 1).getNumeProdus(),
					Integer.parseInt(val[2].substring(1)), price);

			return sfinal;
		}

		GeneratePDF pf = new GeneratePDF();
		pf.noCreateBill();
		return null;
	}

	private String split_del_product(String s) {

		String[] values = s.split(":");
		String sfinal = "numeProdus=" + "'" + values[1] + "'";
		return sfinal;
	}

	private String split_del_client(String s) {

		String[] values = s.split(":");
		String[] val = values[1].split(",");
		int i1 = indexClient(val[0]);
		String sfinal = "numeClient=" + "'" + val[0] + "'" + " and " + "adresaClient=" + "'" + val[1] + "'";
		clienti.remove(i1 - 1);

		return sfinal;
	}

}
