package presentation;

import java.io.FileOutputStream;
import java.util.ArrayList;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import dao.*;
import model.Clienti;
import model.OrderItem;
import model.Produs;;

public class GeneratePDF {
      
	
	static int pdfRapClienti=1;
	static int pdfRapProduse=1;
	static int pdfRapComenzi=1;
	static int pdfBill=1;
	
	public void raportProductPDF(ArrayList<Produs> pr) {
		try {
			String file_name = "C:\\generate\\raportProduse"+pdfRapProduse;
			pdfRapProduse++;
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(file_name));

			document.open();

			Paragraph p = new Paragraph("Report Produs");
			Paragraph space = new Paragraph("   ");

			PdfPTable table = new PdfPTable(4);

			PdfPCell c1 = new PdfPCell(new Phrase("idProdus"));

			PdfPCell c2 = new PdfPCell(new Phrase("numeProdus"));

			PdfPCell c3 = new PdfPCell(new Phrase("cantitateProdus"));

			PdfPCell c4 = new PdfPCell(new Phrase("pretProdus"));

			table.addCell(c1);
			table.addCell(c2);
			table.addCell(c3);
			table.addCell(c4);
			for (int i = 0; i < pr.size(); i++) {
				c1 = new PdfPCell(new Phrase("" + pr.get(i).getIdProdus()));

				c2 = new PdfPCell(new Phrase(pr.get(i).getNumeProdus()));

				c3 = new PdfPCell(new Phrase("" + pr.get(i).getCantitateProdus()));
				c4 = new PdfPCell(new Phrase("" + pr.get(i).getPretProdus()));
				table.addCell(c1);
				table.addCell(c2);
				table.addCell(c3);
				table.addCell(c4);

			}

			document.add(p);
			document.add(space);
			document.add(table);

			document.close();

		} catch (Exception e) {
			System.out.println("error");
		}
	}

	public void raportClientPDF(ArrayList<Clienti> cl) {
		try {
			String file_name = "C:\\generate\\raportClienti"+pdfRapClienti;
			pdfRapClienti++;
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(file_name));
			Parsare fis = new Parsare();
			/*
			 * ArrayList<Clienti> cl = new ArrayList<Clienti>(); cl = fis.afisClienti();
			 */

			document.open();

			Paragraph p = new Paragraph("Report Client");
			Paragraph space = new Paragraph("   ");

			PdfPTable table = new PdfPTable(3);

			PdfPCell c1 = new PdfPCell(new Phrase("idClient"));

			PdfPCell c2 = new PdfPCell(new Phrase("numeClient"));

			PdfPCell c3 = new PdfPCell(new Phrase("adresaClient"));

			table.addCell(c1);
			table.addCell(c2);
			table.addCell(c3);

			for (int i = 0; i < cl.size(); i++) {
				c1 = new PdfPCell(new Phrase("" + cl.get(i).getIdClient()));

				c2 = new PdfPCell(new Phrase(cl.get(i).getNumeClient()));

				c3 = new PdfPCell(new Phrase(cl.get(i).getAdresaClient()));
				table.addCell(c1);
				table.addCell(c2);
				table.addCell(c3);

			}
			document.add(p);
			document.add(space);
			document.add(table);

			document.close();

		} catch (Exception e) {
			System.out.println("error");
		}
	}

	public void raportOrderPDF(ArrayList<OrderItem> cm) {
		try {
			String file_name = "C:\\generate\\raportComenzi"+pdfRapComenzi;
			
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(file_name));

			document.open();

			Paragraph p = new Paragraph("Report Orders");
			Paragraph space = new Paragraph("   ");

			PdfPTable table = new PdfPTable(5);

			PdfPCell c1 = new PdfPCell(new Phrase("idOrderItem"));

			PdfPCell c2 = new PdfPCell(new Phrase("idProdus"));

			PdfPCell c3 = new PdfPCell(new Phrase("idClient"));

			PdfPCell c4 = new PdfPCell(new Phrase("cantitateOrderItem"));

			PdfPCell c5 = new PdfPCell(new Phrase("pretOrderItem"));

			table.addCell(c1);
			table.addCell(c2);
			table.addCell(c3);
			table.addCell(c4);
			table.addCell(c5);
			for (int i = 0; i < cm.size(); i++) {
				c1 = new PdfPCell(new Phrase("" + cm.get(i).getIdOrderItem()));
				c2 = new PdfPCell(new Phrase("" + cm.get(i).getIdProdus()));
				c3 = new PdfPCell(new Phrase("" + cm.get(i).getIdClient()));
				c4 = new PdfPCell(new Phrase("" + cm.get(i).getCantitateOrderItem()));
				c5 = new PdfPCell(new Phrase("" + cm.get(i).getPretOrderItem()));

				table.addCell(c1);
				table.addCell(c2);
				table.addCell(c3);
				table.addCell(c4);
				table.addCell(c5);

			}

			document.add(p);
			document.add(space);
			document.add(table);
			pdfRapComenzi++;

			document.close();

		} catch (Exception e) {
			System.out.println("error");
		}
	}
	public void createBill(String numeClient,String numeProdus,int cantitate,double pret) {
		try {
			String file_name = "C:\\generate\\raportBill"+pdfBill;
			
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(file_name));

			document.open();

			Paragraph p = new Paragraph("Bill no"+pdfBill);
			Paragraph space = new Paragraph("   ");
			Paragraph bill = new Paragraph(numeClient+" "+numeProdus+" "+cantitate+" "+pret);
			document.add(p);
			document.add(space);
			document.add(bill);
			document.close();
			pdfBill++;
		}
		catch (Exception e) {
			System.out.println("error");
		}
	}	
	public void noCreateBill() {
		try {
			String file_name = "C:\\generate\\raportBill"+pdfBill;
			
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(file_name));

			document.open();

			Paragraph p = new Paragraph("Bill no"+pdfBill);
			Paragraph space = new Paragraph("   ");
			Paragraph bill = new Paragraph("under-stock");
			document.add(p);
			document.add(space);
			document.add(bill);
			document.close();
			pdfBill++;
		}
		catch (Exception e) {
			System.out.println("error");
		}
	}	

}
